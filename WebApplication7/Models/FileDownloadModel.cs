﻿namespace WebApplication7.Models
{
    public class FileDownloadModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FileName { get; set; }
        public override string ToString()
        {
            return $"FirstName: {FirstName}, LastName: {LastName}, FileName: {FileName}.txt";
        }
    }
}
