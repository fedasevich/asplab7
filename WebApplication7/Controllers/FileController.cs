﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text;
using WebApplication7.Models;

namespace WebApplication7.Controllers
{
    public class FileController : Controller
    {
        [HttpGet]
        public IActionResult DownloadFile()
        {
            return View();
        }

        [HttpPost]
        public IActionResult DownloadFile(FileDownloadModel fileDownloadModel)
        {
            try
            {

                string file_type = "text/plain";

                string file_name = $"{fileDownloadModel.FileName}.txt";

                byte[] data = Encoding.UTF8.GetBytes(fileDownloadModel.ToString());

                return File(data, file_type, file_name);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
